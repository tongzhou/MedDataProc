
DATA WORK.PARTD_PUF_NPI_SUMMARY_2014;
	LENGTH
		npi									$ 10
		nppes_provider_last_org_name 		$ 70
		nppes_provider_first_name 			$ 20
		nppes_provider_mi					$  1
		nppes_credentials 					$ 20
		nppes_provider_gender				$  1
		nppes_entity_code 					$  1
		nppes_provider_street1 				$ 55
		nppes_provider_street2				$ 55
		nppes_provider_city 				$ 40
		nppes_provider_zip5 				$  5
		nppes_provider_zip4					$  4
		nppes_provider_state				$  2
		nppes_provider_country				$  2
		specialty_description				$ 75
		description_flag				 	$  1
		medicare_prvdr_enroll_status		$  1
		bene_count							   8
		total_claim_count 					   8
		total_drug_cost 					   8
		total_day_supply 					   8
		bene_count_ge65						   8
		bene_count_ge65_suppress_flag		$  1
		total_claim_count_ge65				   8
		ge65_suppress_flag					$  1
		total_drug_cost_ge65				   8
		total_day_supply_ge65				   8
		brand_claim_count 					   8
		brand_suppress_flag					$  1
		brand_drug_cost 					   8
		generic_claim_count 				   8
		generic_suppress_flag				$  1
		generic_drug_cost 					   8
		other_claim_count 					   8
		other_suppress_flag					$  1
		other_drug_cost 					   8
		mapd_claim_count					   8
		mapd_suppress_flag					$  1
		mapd_drug_cost						   8
		pdp_claim_count						   8
		pdp_suppress_flag					$  1
		pdp_drug_cost						   8
		lis_claim_count						   8
		lis_suppress_flag					$  1
		lis_drug_cost 						   8
		nonlis_claim_count					   8
		nonlis_suppress_flag				$  1
		nonlis_drug_cost				 	   8
		opioid_bene_count					   8
		opioid_claim_count					   8
		opioid_drug_cost 					   8
		opioid_day_supply					   8
		antibiotic_bene_count  				   8
		antibiotic_claim_count 				   8
		antibiotic_drug_cost 				   8 
		hrm_bene_count_ge65					   8 
		hrm_bene_ge65_suppress_flag			$  1
		hrm_claim_count_ge65				   8
		hrm_ge65_suppress_flag				$  1
		hrm_drug_cost_ge65					   8
		anti_psych_bene_count 				   8
		anti_psych_claim_count 				   8
		anti_psych_drug_cost 				   8

	;

	INFILE 'c:\My documents\PartD_Prescriber_PUF_NPI_14.txt'
		dlm='09'x
		pad missover
		firstobs = 2
		dsd
	;

	INPUT
		npi							
		nppes_provider_last_org_name
		nppes_provider_first_name 	
		nppes_provider_mi			
		nppes_credentials 			
		nppes_provider_gender		
		nppes_entity_code 			
		nppes_provider_street1 		
		nppes_provider_street2		
		nppes_provider_city 		
		nppes_provider_zip5
		nppes_provider_zip4	
		nppes_provider_state		
		nppes_provider_country		
		specialty_description		
		description_flag			
		medicare_prvdr_enroll_status
		bene_count					
		total_claim_count 			
		total_drug_cost 			
		total_day_supply 			
		bene_count_ge65				
		bene_count_ge65_suppress_flag	
		total_claim_count_ge65		
		ge65_suppress_flag			
		total_drug_cost_ge65		
		total_day_supply_ge65		
		brand_claim_count 			
		brand_suppress_flag			
		brand_drug_cost 			
		generic_claim_count 		
		generic_suppress_flag			
		generic_drug_cost 			
		other_claim_count 			
		other_suppress_flag			
		other_drug_cost 			
		mapd_claim_count			
		mapd_suppress_flag			
		mapd_drug_cost				
		pdp_claim_count				
		pdp_suppress_flag				
		pdp_drug_cost				
		lis_claim_count				
		lis_suppress_flag				
		lis_drug_cost 				
		nonlis_claim_count			
		nonlis_suppress_flag			
		nonlis_drug_cost
		opioid_bene_count
		opioid_claim_count
		opioid_drug_cost
		opioid_day_supply
		antibiotic_bene_count
		antibiotic_claim_count 	
		antibiotic_drug_cost
		hrm_bene_count_ge65
		hrm_bene_ge65_suppress_flag
		hrm_claim_count_ge65
		hrm_ge65_suppress_flag
		hrm_drug_cost_ge65
		anti_psych_bene_count
		anti_psych_claim_count
		anti_psych_drug_cost
	;	

	LABEL
		npi								= "National Provider Identifier"
		nppes_provider_last_org_name 	= "Last Name/Organization Name of the Provider"
		nppes_provider_first_name 		= "First Name of the Provider"
		nppes_provider_mi				= "Middle Initial of the Provider"
		nppes_credentials 				= "Credentials of the Provider"
		nppes_provider_gender			= "Gender of the Provider"
		nppes_entity_code 				= "Entity Type of the Provider"
		nppes_provider_street1 			= "Street Address 1 of the Provider"
		nppes_provider_street2			= "Street Address 2 of the Provider"
		nppes_provider_city 			= "City of the Provider"
		nppes_provider_zip5				= "Zip Code of the Provider (first five digits)"
		nppes_provider_zip4				= "Zip Code of the Provider (last four digits)"
		nppes_provider_state			= "State Code of the Provider"
		nppes_provider_country			= "Country Code of the Provider"
		specialty_description			= "Provider Specialty Type"
		description_flag				= "Source of Provider Specialty"
		medicare_prvdr_enroll_status	= "Enrollment Status of the Provider in the Medicare Program"
		bene_count						= "Number of Medicare Beneficiaries"
		total_claim_count 				= "Number of Medicare Part D Claims, Including Refills"
		total_drug_cost 				= "Aggregate Cost Paid for All Claims"
		total_day_supply 				= "Number of Day's Supply for All Claims"
		bene_count_ge65					= "Number of Medicare Beneficiaries Age 65+"
		bene_count_ge65_suppress_flag	= "Reason for Suppression of Bene_Count_ge65"
		total_claim_count_ge65			= "Number of Claims, Including Refills, for Beneficiaries Age 65+"
		ge65_suppress_flag				= "Reason for Suppression of Total_Claim_Count_Ge65, Total_Drug_Cost_Ge65, and Total_Day_Supply_Ge65"
		total_drug_cost_ge65			= "Aggregate Cost Paid for All Claims for Beneficiaries Age 65+"
		total_day_supply_ge65			= "Number of Day's Supply for All Claims for Beneficiaries Age 65+"
		brand_claim_count 				= "Total Claims of Brand-Name Drugs, Including Refills"
		brand_suppress_flag				= "Reason for Suppression of Brand_Claim_Count and Brand_Drug_Cost"
		brand_drug_cost 				= "Aggregate Cost Paid for Brand-Name Drugs"	
		generic_claim_count 			= "Total Claims of Generic Drugs, Including Refills"
		generic_suppress_flag			= "Reason for Suppression of Generic_Claim_Count and Generic_Drug_Cost"
		generic_drug_cost				= "Aggregate Cost Paid for Generic Drugs"
		other_claim_count				= "Total Claims of Other Drugs, Including Refills"		
		other_suppress_flag				= "Reason for Suppression of Other_Claim_Count and Other_Drug_Cost"
		other_drug_cost 				= "Aggregate Cost Paid for Other Drugs"
		mapd_claim_count				= "Number of Claims for Beneficiaries Covered by MAPD Plans"
		mapd_suppress_flag				= "Reason for Suppression of MAPD_Claim_Count and MAPD_Drug_Cost"
		mapd_drug_cost					= "Aggregate Cost Paid for Claims Filled by Beneficiaries in MAPD Plans"
		pdp_claim_count					= "Number of Claims for Beneficiaries Covered by Standalone PDP Plans"
		pdp_suppress_flag				= "Reason for Suppression of PDP_Claim_Count and PDP_Drug_Cost"
		pdp_drug_cost					= "Aggregate Cost Paid for Claims Filled by Beneficiaries in Standalone PDP Plans"
		lis_claim_count					= "Number of Claims for Beneficiaries Covered by Low-Income Subsidy"
		lis_suppress_flag				= "Reason for Suppression of Lis_Claim_Count and Lis_Drug_Cost"
		lis_drug_cost 					= "Aggregate Cost Paid for Claims Covered by Low-Income Subsidy"
		nonlis_claim_count				= "Number of Claims for Beneficiaries Not Covered by Low-Income Subsidy"	
		nonlis_suppress_flag			= "Reason for Suppression of Nonlis_Claim_Count and Nonlis_Drug_Cost"
		nonlis_drug_cost				= "Aggregate Cost Paid for Claims Not Covered by Low-Income Subsidy"
		opioid_bene_count				= "Number of Medicare Beneficiaries Filling Opioid Claims"
		opioid_claim_count				= "Total Claims of Opioid Drugs, Including Refills"
		opioid_drug_cost 				= "Aggregate Cost Paid for Opioid Drugs"
		opioid_day_supply				= "Number of Day's Supply of All Opioid Drugs"
		antibiotic_bene_count  			= "Number of Medicare Beneficiaries Filling Antibiotic Claims"
		antibiotic_claim_count 			= "Total Claims of Antibiotic Drugs, Including Refills"
		antibiotic_drug_cost 			= "Aggregate Cost Paid for Antibiotic Drugs"
		hrm_bene_count_ge65				= "Number of Medicare Beneficiaries Age 65+ Filling HRM Claims"
		hrm_bene_ge65_suppress_flag		= "Reason for Suppression of HRM_Bene_Count_GE65"
		hrm_claim_count_ge65			= "Total Claims of HRM Drugs, Including Refills, for Beneficiaries Age 65+"
		hrm_ge65_suppress_flag			= "Reason for Suppression of HRM_Claim_Count_Ge65 and HRM_Drug_Cost_Ge65"
		hrm_drug_cost_ge65				= "Aggregate Cost Paid for HRM Drugs for Beneficiaries Age 65+"
		anti_psych_bene_count 			= "Number of Medicare Beneficiaries Filling Antipsychotic Claims"
		anti_psych_claim_count 			= "Total Claims of Antipsychotic Drugs, Including Refills"
		anti_psych_drug_cost 			= "Aggregate Cost Paid for Antipsychotic Drugs"
	;

RUN;