import numpy as np

VERBOSE = True
def checkKey(list, word):
    res = []
    for i, each in enumerate(list):
        if word in each:
            res.append((i, each))
    print res

def proc():
    bene_cnt_id = 17
    total_bene_cnt = 0.0
    bene_pi_cnt = 0.0

    total_day_supply_id = 20
    total_claim_count_id = 18
    total_pres_len = 0.0
    pres_pi_cnt = 0.0
    pres_lens = []


    line_num = 0
    col_num = 0
    header = []
    for line in open("PartD_Prescriber_PUF_NPI_14/PartD_Prescriber_PUF_NPI_14.txt"):
        line_num += 1
        if line_num == 1:
            header = [x.lower() for x in line.strip().split('\t')]
            col_num = len(header)
            #checkKey(header, 'specialty')
            continue

        items = line.replace('\n', '').split('\t')

        if items[bene_cnt_id] != '':
            this_bene_cnt = float(items[bene_cnt_id])
            total_bene_cnt += this_bene_cnt
            bene_pi_cnt += 1

        if items[total_day_supply_id] != '' and items[total_claim_count_id] != '':
            this_pres_len = float(items[total_day_supply_id])/float(items[total_claim_count_id])
            total_pres_len += this_pres_len
            pres_pi_cnt += 1
            pres_lens.append(this_pres_len)

        if line_num == 300:
            #exit(0)
            pass


    avg_bene_cnt = total_bene_cnt/bene_pi_cnt
    avg_pres_len = total_pres_len/pres_pi_cnt
    median_pres_len = np.median(np.array(pres_lens))

    print 'average bene_count:', avg_bene_cnt
    print 'average of length of average prescription:', avg_pres_len
    print 'median of length of average prescription:', median_pres_len


    if VERBOSE:
        print 'line number:', line_num

if __name__ == "__main__":
    proc()
