import numpy as np

VERBOSE = True
def checkKey(list, word):
    res = []
    for i, each in enumerate(list):
        if word in each:
            res.append((i, each))
    print res

def proc():
    line_num = 0
    npis = {}

    npi_id = 0
    specialty_id = 13
    specs_13 = {}
    for line in open("PartD_Prescriber_PUF_NPI_13/PARTD_PRESCRIBER_PUF_NPI_13.tab"):
        line_num += 1
        if line_num == 1:
            header = [x.lower() for x in line.strip().split('\t')]
            col_num = len(header)
            checkKey(header, 'specialty')
            continue

        items = line[:-1].split('\t')

        npi_, spec_ = items[npi_id], items[specialty_id]
        if spec_ not in specs_13:
            specs_13[spec_] = [0.0, 0.0]
        specs_13[spec_][0] += 1

        npis[int(npi_)] = [spec_]


    specialty_id = 14
    total_day_supply_id = 20


    line_num = 0
    header = []
    for line in open("PartD_Prescriber_PUF_NPI_14/PartD_Prescriber_PUF_NPI_14.txt"):
        line_num += 1
        if line_num == 1:
            header = [x.lower() for x in line.strip().split('\t')]
            col_num = len(header)
            checkKey(header, 'specialty')
            continue

        items = line[:-1].split('\t')

        npi_, spec_ = items[npi_id], items[specialty_id]

        if int(npi_) in npis:
            npis[int(npi_)].append(spec_)

    for npi, ss in npis.iteritems():
        if len(ss) == 1:
            specs_13[ss[0]][1] += 1

        elif ss[0] != ss[1]:
            specs_13[ss[0]][1] += 1

    fracs = []
    for spec, (total, changed) in specs_13.iteritems():
        if total >= 1000:
            #print (total, changed)
            fracs.append((spec, changed/total))

    fracs = sorted(fracs, key=lambda x: x[1], reverse=True)
    print fracs[0]

if __name__ == "__main__":
    proc()
