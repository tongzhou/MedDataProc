
DATA WORK.PARTD_PUF_NPI_SUMMARY_2013;
	LENGTH
		npi									$ 10
		nppes_provider_last_org_name 		$ 70
		nppes_provider_first_name 			$ 20
		nppes_provider_mi					$  1
		nppes_credentials 					$ 20
		nppes_provider_gender				$  1
		nppes_entity_code 					$  1
		nppes_provider_street1 				$ 55
		nppes_provider_street2				$ 55
		nppes_provider_city 				$ 40
		nppes_provider_zip 					$ 20
		nppes_provider_state				$  2
		nppes_provider_country				$  2
		specialty_description				$ 91
		description_flag				 	$  1
		bene_count							   8
		total_claim_count 					   8
		total_drug_cost 					   8
		total_day_supply 					   8
		bene_count_ge65						   8
		bene_count_ge65_redact_flag			$  1
		total_claim_count_ge65				   8
		ge65_redact_flag					$  1
		total_drug_cost_ge65				   8
		total_day_supply_ge65				   8
		brand_claim_count 					   8
		brand_redact_flag					$  1
		brand_drug_cost 					   8
		generic_claim_count 				   8
		generic_redact_flag					$  1
		generic_drug_cost 					   8
		other_claim_count 					   8
		other_redact_flag					$  1
		other_drug_cost 					   8
		mapd_claim_count					   8
		mapd_redact_flag					$  1
		mapd_drug_cost						   8
		pdp_claim_count						   8
		pdp_redact_flag						$  1
		pdp_drug_cost						   8
		lis_claim_count						   8
		lis_redact_flag						$  1
		lis_drug_cost 						   8
		nonlis_claim_count					   8
		nonlis_redact_flag					$  1
		nonlis_drug_cost				 	   8
	;

	INFILE 'c:\My documents\PartD_Prescriber_PUF_NPI_13.txt'
		dlm='09'x
		pad missover
		firstobs = 2
		dsd
	;

	INPUT
		npi							
		nppes_provider_last_org_name
		nppes_provider_first_name 	
		nppes_provider_mi			
		nppes_credentials 			
		nppes_provider_gender		
		nppes_entity_code 			
		nppes_provider_street1 		
		nppes_provider_street2		
		nppes_provider_city 		
		nppes_provider_zip 			
		nppes_provider_state		
		nppes_provider_country		
		specialty_description		
		description_flag			
		bene_count					
		total_claim_count 			
		total_drug_cost 			
		total_day_supply 			
		bene_count_ge65				
		bene_count_ge65_redact_flag	
		total_claim_count_ge65		
		ge65_redact_flag			
		total_drug_cost_ge65		
		total_day_supply_ge65		
		brand_claim_count 			
		brand_redact_flag			
		brand_drug_cost 			
		generic_claim_count 		
		generic_redact_flag			
		generic_drug_cost 			
		other_claim_count 			
		other_redact_flag			
		other_drug_cost 			
		mapd_claim_count			
		mapd_redact_flag			
		mapd_drug_cost				
		pdp_claim_count				
		pdp_redact_flag				
		pdp_drug_cost				
		lis_claim_count				
		lis_redact_flag				
		lis_drug_cost 				
		nonlis_claim_count			
		nonlis_redact_flag			
		nonlis_drug_cost			
	;	

	LABEL
		npi								= "National Provider Identifier"
		nppes_provider_last_org_name 	= "Last Name/Organization Name"
		nppes_provider_first_name 		= "First Name"
		nppes_provider_mi				= "Middle Initial"
		nppes_credentials 				= "Credentials"
		nppes_provider_gender			= "Gender"
		nppes_entity_code 				= "Entity Code"
		nppes_provider_street1 			= "Street Address 1"
		nppes_provider_street2			= "Street Address 2"
		nppes_provider_city 			= "City"
		nppes_provider_zip 				= "Zip Code"
		nppes_provider_state			= "State Code"
		nppes_provider_country			= "Country Code"
		specialty_description			= "Provider Specialty Type"
		description_flag				= "Source of Provider Specialty"
		bene_count						= "Number of Medicare Beneficiaries"
		total_claim_count 				= "Number of Medicare Part D claims, including refills."
		total_drug_cost 				= "Aggregate cost paid for all claims."
		total_day_supply 				= "Number of days supply for all claims."
		bene_count_ge65					= "Number of Medicare Beneficiaries, aged 65 or older"
		bene_count_ge65_redact_flag		= "Flag detailing reason for redaction of bene_count_ge65 field"
		total_claim_count_ge65			= "Number of claims, including refills, where the beneficiary was 65 or older."
		ge65_flag						= "Flag detailing reason for redaction of total_claim_count_ge65 field"
		total_drug_cost_ge65			= "Aggregate cost paid for all claims, where the beneficiary is 65 or older."
		total_day_supply_ge65			= "Number of days supply for all claims, where the beneficiary was 65 or older."
		brand_claim_count 				= "Total claims of brand-name drugs, including refills."
		brand_redact_flag				= "Flag detailing reason for redaction of brand_claim_count field"
		brand_drug_cost 				= "Aggregate cost paid for brand-name drugs."	
		generic_claim_count 			= "Total claims of generic drugs, including refills."
		generic_redact_flag				= "Flag detailing reason for redaction of generic_claim_count field"
		generic_drug_cost				= "Aggregate cost paid for generic drugs."
		other_claim_count				= "Total claims of other drugs, including refills."		
		other_redact_flag				= "Flag detailing reason for redaction of other_claim_count field"
		other_drug_cost 				= "Aggregate cost paid for other drugs."
		mapd_claim_count				= "Number of claims for beneficiaries covered by Medicare Advantage plans."
		mapd_redact_flag				= "Flag detailing reason for redaction of mapd_claim_count field"
		mapd_drug_cost					= "Aggregate cost paid for claims filled by beneficiaries in Medicare Advantage plans."
		pdp_claim_count					= "Number of claims for beneficiaries covered by standalone prescription drug plans (not Medicare Advantage plans)."
		pdp_redact_flag					= "Flag detailing reason for redaction of pdp_claim_count field"
		pdp_drug_cost					= "Aggregate cost paid for claims filled by beneficiaries in standalone prescription drug plans (not Medicare Advantage plans)."
		lis_claim_count					= "Number of claims for beneficiaries covered by the low-income subsidy."
		lis_redact_flag					= "Flag detailing reason for redaction of lis_claim_count field"
		lis_drug_cost 					= "Aggregate cost paid for claims that were covered by the low-income subsidy."
		nonlis_claim_count				= "Number of claims for beneficiaries not covered by the low-income subsidy."	
		nonlis_redact_flag				= "Flag detailing reason for redaction of nonlis_claim_count field"
		nonlis_drug_cost				= "Aggregate cost paid for claims that were not covered by the low-income subsidy."
	;

RUN;
