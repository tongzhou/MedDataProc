import numpy as np

VERBOSE = True
def checkKey(list, word):
    res = []
    for i, each in enumerate(list):
        if word in each:
            res.append((i, each))
    print res

def proc():
    nppes_provider_state_id = 12
    opioid_bene_cnt_id = 48
    antibiotic_bene_cnt_id = 52

    states = {}

    line_num = 0
    header = []
    for line in open("PartD_Prescriber_PUF_NPI_14/PartD_Prescriber_PUF_NPI_14.txt"):
        line_num += 1
        if line_num == 1:
            header = [x.lower() for x in line.strip().split('\t')]
            col_num = len(header)
            #checkKey(header, 'antibio')
            continue

        items = line[:-1].split('\t')

        state_, opioid_, antib_ = items[nppes_provider_state_id], items[opioid_bene_cnt_id], items[antibiotic_bene_cnt_id]
        if state_ != '' and opioid_ != '' and antib_ != '':
            if state_ not in states:
                states[state_] = [0.0, 0.0]
            states[state_][0] += float(opioid_)
            states[state_][1] += float(antib_)

        if line_num == 3:
            #exit(0)
            pass

    ratios = []
    for state, sums in states.iteritems():
        ratio = sums[0]/sums[1]
        ratios.append(ratio)
        print "state: %s, ratio: %.3f" % (state, ratio)

    print "max ratio: %.3f, min ratio: %.3f" % (max(ratios), min(ratios))
    print "max ratio - min ratio: %.3f" % (max(ratios) - min(ratios))


if __name__ == "__main__":
    proc()
