import numpy as np

VERBOSE = True
def checkKey(list, word):
    res = []
    for i, each in enumerate(list):
        if word in each:
            res.append((i, each))
    print res

def proc():
    total_drug_cost_id = 19
    total_day_supply_id = 20
    costs_14 = []

    line_num = 0
    header = []
    for line in open("PartD_Prescriber_PUF_NPI_14/PartD_Prescriber_PUF_NPI_14.txt"):
        line_num += 1
        if line_num == 1:
            header = [x.lower() for x in line.strip().split('\t')]
            col_num = len(header)
            checkKey(header, 'total_day_supply')
            continue

        items = line[:-1].split('\t')

        x, y = items[total_drug_cost_id], items[total_day_supply_id]
        if x != '' and y != '':
            costs_14.append(float(x)/float(y))

    line_num = 0
    total_drug_cost_id = 17
    total_day_supply_id = 18
    costs_13 = []
    for line in open("PartD_Prescriber_PUF_NPI_13/PARTD_PRESCRIBER_PUF_NPI_13.tab"):
        line_num += 1
        if line_num == 1:
            header = [x.lower() for x in line.strip().split('\t')]
            col_num = len(header)
            checkKey(header, 'drug_cost')
            continue

        items = line[:-1].split('\t')

        x, y = items[total_drug_cost_id], items[total_day_supply_id]
        if x != '' and y != '':
            costs_13.append(float(x)/float(y))


    avg_14 = sum(costs_14)/len(costs_14)
    avg_13 = sum(costs_13)/len(costs_13)
    print "13: %.3f, 14: %.3f, ratio: %.3f" % (avg_13, avg_14, avg_14/avg_13)

if __name__ == "__main__":
    proc()
