import numpy as np
import scipy.stats

VERBOSE = True
def checkKey(list, word):
    res = []
    for i, each in enumerate(list):
        if word in each:
            res.append((i, each))
    print res

def proc():
    total_claim_cnt_ge65_id = 23
    total_claim_count_id = 18
    lis_claim_count_id = 42

    ageds = []
    liss = []

    line_num = 0
    header = []
    for line in open("PartD_Prescriber_PUF_NPI_14/PartD_Prescriber_PUF_NPI_14.txt"):
        line_num += 1
        if line_num == 1:
            header = [x.lower() for x in line.strip().split('\t')]
            col_num = len(header)
            #checkKey(header, 'ge65')
            continue

        items = line[:-1].split('\t')

        x, y, z = items[total_claim_cnt_ge65_id], items[total_claim_count_id], items[lis_claim_count_id]

        if x != '' and y != '' and z != '':
            ageds.append(float(x)/float(y))
            liss.append(float(z)/float(y))

        if line_num == 3:
            #exit(0)
            pass

    cor = scipy.stats.pearsonr(ageds, liss)
    print cor
    print "The Pearson correlation coefficient is %.3f" % cor[0]

if __name__ == "__main__":
    proc()
