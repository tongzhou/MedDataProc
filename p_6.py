import numpy as np

VERBOSE = True
def checkKey(list, word):
    res = []
    for i, each in enumerate(list):
        if word in each:
            res.append((i, each))
    print res

def proc():
    opioid_bene_cnt_id = 48
    opioid_claim_cnt_id = 49
    opioid_day_supply_id = 51
    state_id = 12
    specialty_id = 14
    npi_id = 0
    npi_dc = {}
    spec_dc = {}

    line_num = 0
    header = []
    for line in open("PartD_Prescriber_PUF_NPI_14/PartD_Prescriber_PUF_NPI_14.txt"):
        line_num += 1
        if line_num == 1:
            header = [x.lower() for x in line.strip().split('\t')]
            col_num = len(header)
            checkKey(header, 'special')
            continue

        items = line[:-1].split('\t')

        x, y = items[opioid_day_supply_id], items[opioid_claim_cnt_id]
        state_, spec_ = items[state_id], items[specialty_id]
        if x != '' and x != '0':
            pres_len = float(x)/float(y)
            if items[npi_id] in npi_dc:
                print "should't happen"

            npi_dc[items[npi_id]] = (state_, spec_, pres_len)


        if line_num == 30:
            #exit(0)
            pass

    ## (state, spec) => (provider_num, total_len)
    sspairs = {}
    for npi, (state_, spec_, pres_len) in npi_dc.iteritems():
        if (state_, spec_) not in sspairs:
            sspairs[(state_, spec_)] = [0.0, 0.0]
        sspairs[(state_, spec_)][0] += 1
        sspairs[(state_, spec_)][1] += pres_len

        if spec_ not in spec_dc:
            spec_dc[spec_] = []
        spec_dc[spec_].append(pres_len)

    specavg_dc = {}
    for spec_, ls in spec_dc.iteritems():
        specavg_dc[spec_] = sum(ls) / len(ls)

    newsspairs = {}
    ratios = []
    for (state_, spec_), (provider_num, total_len) in sspairs.iteritems():
        if provider_num >= 100:
            newsspairs[(state_, spec_)] = total_len/provider_num

    for spec in specavg_dc:
        states = []
        #print spec
        for (state_, spec_), avg_len in newsspairs.iteritems():
            if spec == spec_:
                states.append((state_, avg_len/specavg_dc[spec]))
        if len(states) == 0:
            #print "Not such state specialty pair having more than 100 providers"
            continue

        states = sorted(states, key=lambda x: x[1], reverse=True)
        print "specialty:", spec
        print "state that has the highest ratio:", states[0]
        print






if __name__ == "__main__":
    proc()
