import numpy as np

VERBOSE = True
def checkKey(list, word):
    res = []
    for i, each in enumerate(list):
        if word in each:
            res.append((i, each))
    print res

def proc():
    spec_desc_id = 14
    total_claim_count_id = 18
    brand_claim_count_id = 27
    specs = {}

    line_num = 0
    col_num = 0
    header = []
    for line in open("PartD_Prescriber_PUF_NPI_14/PartD_Prescriber_PUF_NPI_14.txt"):
        line_num += 1
        if line_num == 1:
            header = [x.lower() for x in line.strip().split('\t')]
            col_num = len(header)
            #checkKey(header, 'specialty')
            continue

        items = line[:-1].split('\t')

        spec_, brand_claim_cnt_, total_claim_cnt_ = items[spec_desc_id], items[brand_claim_count_id], items[total_claim_count_id]


        if spec_ != 0 and brand_claim_cnt_ != '' and total_claim_cnt_ != '':
            if float(total_claim_cnt_) > 1000:
                frac_ = float(brand_claim_cnt_)/float(total_claim_cnt_)
                if spec_ not in specs:
                    specs[spec_] = [0.0, 0.0]
                specs[spec_][0] += float(brand_claim_cnt_)
                specs[spec_][1] += float(total_claim_cnt_)


        if line_num == 3:
            #exit(0)
            pass

    spec_avgs = []
    for spec, (brand, total) in specs.iteritems():
        spec_avgs.append(brand/total)

    nplist = np.array(spec_avgs)
    print 'average: %.3f, standard deviation: %.3f' % (np.average(nplist), np.std(nplist))
    #print 'specialty: %s, average: %.3f, standard deviation: %.3f' % (spec, np.average(nplist), np.std(nplist))

    #print 'the average value of fraction for each specialty', avg_spec_fraction
    #print 'the standard deviation of fraction for each specialty', std_spec_fraction

    if VERBOSE:
        print 'line number:', line_num

if __name__ == "__main__":
    proc()
